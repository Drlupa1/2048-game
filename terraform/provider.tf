terraform {
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
    }
  }
}

provider "yandex" {
  service_account_key_file = "key.json"
  cloud_id                 = "b1gcfe1f92he3et9pdj7"
  folder_id                = "b1g1q11q27ohdh1p0c45"
  zone                     = "ru-central1-a"
}