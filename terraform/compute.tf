resource "yandex_compute_instance" "game2048" {
  name        = "game"
  platform_id = "standard-v2"
  zone        = "ru-central1-a"

  resources {
    core_fraction = 5
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params{
      image_id = "fd8ba9d5mfvlncknt2kd"
    }
  }

  network_interface {
    subnet_id = "e9bcmf1isedsd4nctaot"
    nat = true
  }

  metadata =  {
    user-data = "${file("cloud-init.yml")}"
  }

  scheduling_policy {
    preemptible = true
  }
}