Install role 
------------
```yaml
- name: docker
  src: git+https://gitlab.com/galaxy-roles1/docker-galaxy-role.git
  version: v1
```

Example Playbook
----------------
```yaml
- hosts: servers
  gather_facts: true
  become: true
  roles:
    - docker
```

