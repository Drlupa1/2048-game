Install role 
------------
```yaml
- name: nginx
  src: git+https://gitlab.com/galaxy-roles1/nginx-reverse-proxy-galaxy-role.git
  version: v1
```

Example Playbook
----------------
```yaml
- hosts: servers
  gather_facts: true
  become: true
  roles:
    - nginx
```

