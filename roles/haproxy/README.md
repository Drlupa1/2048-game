Install role 
------------
```yaml
- name: haproxy
  src: git+https://gitlab.com/galaxy-roles1/haproxy-galaxy-role.git
  version: v1
```

Example Playbook
----------------
```yaml
- hosts: servers
  gather_facts: true
  become: true
  roles:
    - haproxy
```

